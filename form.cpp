#include "form.h"
#include "ui_form.h"
#include "userlist.h"
#include "chatform.h"
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QDebug>
#include <QNetworkReply>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnCount(1);
    QList<QTreeWidgetItem *> items;
    for (int i = 0; i < 10; ++i)
    {
        QTreeWidgetItem* pItem = new QTreeWidgetItem((QTreeWidget*)0, QStringList(QString("item: %1").arg(i)));
        pItem->setIcon(0, QIcon(QString::fromUtf8(":/ico/pic/INTERNET9.png")));
        items.append(pItem);
    }
    ui->treeWidget->insertTopLevelItems(0, items);
    listWidget = new UserList(ui->tab_2);
    listWidget->SetOwn(this);
    ui->verticalLayout_2->addWidget(listWidget);
    for (int i = 0; i < 10; ++i)
    {
        QListWidgetItem* pItem = new QListWidgetItem;
        pItem->setIcon(QIcon(QString::fromUtf8(":/ico/pic/INTERNET9.png")));
        pItem->setText(QString("item %1").arg(i));
        listWidget->insertItem(i,pItem);
    }
    // 读取文件
    QString runPath = QCoreApplication::applicationDirPath();
    runPath += "/city";
    qDebug() << runPath;
    ReadCityDataFromFile(runPath);
    //CreateHttp();

    // 获取个人所有好友


}

Form::~Form()
{
    delete ui;
}

void Form::replyFinished(QNetworkReply * reply)
{
    QVariant statusCodev = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    QVariant redirectionTargetUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (reply->error() == QNetworkReply::NoError)
    {
        qDebug() << "replay " << reply->readAll();
    }
    else
    {
        qDebug() << "error ";
    }
    reply->deleteLater();

}

void Form::open()
{
    ChatForm *chat = new ChatForm();
    chat->SetOwn(this);
    chat->show();
}

void Form::CreateHttp()
{
    m_networkManager = new QNetworkAccessManager(this);
    connect(m_networkManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(replyFinished(QNetworkReply*)));
    
    m_networkManager->get(QNetworkRequest(QUrl("http://m.weather.com.cn/atad/101230201.html")));
}

void Form::ReadCityDataFromFile(QString)
{
    //
}
