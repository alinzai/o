#include "userlist.h"
#include <QMenu>
#include <QAction>
#include <QString>

UserList::UserList(QWidget *parent) :
    QListWidget(parent),
    m_owner(NULL)
{
}

void UserList::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu* popMenu = new QMenu(this);
    QAction* sendMessageAction = new QAction("发送消息", this);
    popMenu->addAction(sendMessageAction);
    popMenu->addAction(new QAction("查看信息", this));
    if (this->itemAt(mapFromGlobal(QCursor::pos())))
    {
        popMenu->addAction(new QAction("修改", this));
    }
    connect(sendMessageAction, SIGNAL(triggered()), m_owner, SLOT(open()));
    popMenu->exec(QCursor::pos());
}

void UserList::SetOwn(QWidget *owner)
{
    m_owner = owner;
}
