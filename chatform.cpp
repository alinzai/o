#include "chatform.h"
#include "ui_chatform.h"

ChatForm::ChatForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChatForm)
{
    ui->setupUi(this);
    //setAttribute(Qt::WA_DeleteOnClose, true);
}

void ChatForm::SetOwn(QWidget* owner)
{
    m_owner = owner;
}

ChatForm::~ChatForm()
{
    delete ui;
}
