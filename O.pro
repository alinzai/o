######################################################################
# Automatically generated by qmake (1.07a) Tue Jul 1 15:08:17 2014
######################################################################
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += network
TEMPLATE = app
INCLUDEPATH += .

# Input

SOURCES += main.cpp \
    form.cpp \
    userlist.cpp \
    chatform.cpp

HEADERS += \
    form.h \
    userlist.h \
    chatform.h

OTHER_FILES += \
    pic/INTERNET 9.png

RESOURCES += \
    resource.qrc

FORMS += \
    form.ui \
    chatform.ui
