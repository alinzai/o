#ifndef FORM_H
#define FORM_H

#include <QWidget>

class QNetworkAccessManager;
class QNetworkReply;
class UserList;

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();

private:
    Ui::Form *ui;
    UserList * listWidget;
    QNetworkAccessManager *m_networkManager;

    // 建立网格连接
    void CreateHttp();

    void ReadCityDataFromFile(QString);
private slots:
    void replyFinished(QNetworkReply * reply);
    void open();
};

#endif // FORM_H
