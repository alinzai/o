#ifndef CHATFORM_H
#define CHATFORM_H

#include <QWidget>

namespace Ui {
class ChatForm;
}

class ChatForm : public QWidget
{
    Q_OBJECT

public:
    explicit ChatForm(QWidget *parent = 0);
    ~ChatForm();
    void SetOwn(QWidget* owner);
private:
    Ui::ChatForm *ui;

    QWidget *m_owner;
private slots:

};

#endif // CHATFORM_H
