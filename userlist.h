#ifndef USERLIST_H
#define USERLIST_H

#include <QListWidget>

class UserList : public QListWidget
{
    Q_OBJECT
public:
    explicit UserList(QWidget *parent = 0);

    void contextMenuEvent(QContextMenuEvent *event);

    void SetOwn(QWidget *);

signals:

public slots:

private:
    QWidget *m_owner;

};

#endif // USERLIST_H
